# castenforcongress-box

Illinois 6th Congressional District (IL-06 CD) dropbox location finder for Sean Casten's 2020 re-election campaign.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Local Development

Requires the latest stable version of [NodeJS](https://nodejs.org/) (12>).

```sh
$ npm install
$ npm run dev
```

## Updating dropbox locations

All vote-by-mail dropbox locations are sourced from the `locations.csv` file in the `bin` folder. To import your own ballot dropbox list, make sure it has the following columns:

- 1.) A location ID (e.g. county - city or just city) for which you want to look up drop boxes
- 2.) A named location (e.g. Our Town Village Hall) for the dropbox
- 3.) The street address of the dropbox (e.g. 1313 Mockingbird Lane)
- 4.) The city or town name for the dropbox (e.g. Our Town) 
- 5.) The dropbox's ZIP Code (e.g. 60101)
- 6.) The hours that the dropbox is available (e.g. Mon-Fri 08:00 AM - 4:30 PM)
- 7.) Alternative information (e.g. a link to a specific web site, or other location information)
- 8.) Any other detailed instructions

You'll likely need to update `bin/build-ballot-list.js` to have the proper column numbers for your CSV.

Once you're ready to import dropbox data, run the following:

```sh
$ node bin/build-ballot-list.js
$ npm run dev

# Confirm the new data looks correct

$ git add .
$ git commit -m "updated location data"
$ git push origin master

# Deploy your site
```

## Customizing the design & copy

You'll likely need a few hours from a frontend developer to make the changes necessary, but here is a short list of where to start:

- Update `src/Homepage` to have a different Hero image and copy
- Update `src/Nav` to have a different logo
- Update `src/TownPage` to have a different photo and copy
- Update `src/Footer` to have different Q&A copy and disclaimer

## Deployment

We created a static build and hosted it directly within GitLab. We also configured GitLab so that any
incremental change to the site would automatically trigger a rebuild and redeployment of the site. 

To create a static build, just run the following,

```ssh
$ npm run build
```
## Provenance

Kudos to the Ed Markey for Senate (MA) campaign for an easy-to-use code base! We enhanced very little
besides breaking out some of the dropbox location fields for easier formatting within text.

And a big shout-out to Jeff Wishnie and his team at digidems.com for invaluable advice on setting up our
infrastructure within GitLab so we could easily redeploy our site at a moment's notice.
