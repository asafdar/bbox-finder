const fs = require('fs').promises;
const { promisify } = require('util');
const { join } = require('path');

const parse = promisify(require('csv-parse'));

function formatTown(input) {
  const lowercase = input.toLowerCase();
  return `${input[0]}${lowercase.slice(1)}`;
}

function formatZip(input) {
  if (!input) {
    return input;
  }

  const numericOnly = `${input}`.replace(/\D/g,'');

  if (numericOnly.length === 4) {
    return `0${numericOnly}`;
  }

  return numericOnly;
}

(async function() {
  try {
    const csvString = await fs.readFile(join(__dirname, 'locations.csv'));
    const results = await parse(csvString);

    results.shift();

    //  0 = City/Town (Identifier)
    //  1 = Split County Indicator
    //  2 = DropBox Location Name
    //  3 = DropBox Street Address
    //  4 = DropBox City
    //  5 = DropBox ZIP Code
    //  6 = DropBox Dates Open
    //  7 = DropBox Hours Open
    //  8 = DropBox Hours
    //  9 = County Additional Information / Web Site Link
    // 10 = County Clerk Address
    // 11 = County VBM Alert Phone
    // 12 = County VBM Alert E-Mail
    // 13 = Detailed Instructions
    // 14 = No Location Available yet
    const data = results.map((row) => [
      row[0], row[3], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18],
    ]);

    await fs.writeFile(join(__dirname, '../src/data.json'), JSON.stringify(data));
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
})();
